package pik.bor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BorApplication.class, args);
	}
}
